CompSci 307 : Final Project Design Review
===================

> This is the link to the [assignment](http://www.cs.duke.edu/courses/compsci307/current/assign/04_final/):

## Overall Design

#### Entire Project APIs

 * What service does each API provide and how do they work together?
    * Controller
        * (Abstract) DataReader --> Works with the controller to have a completely data driven design. Used for loading and reading all game files.
            * FrontEndDataReader
            * BackEndDataReader
        * DataWriter --> Works with the controller to write all game files. Used in saving and creating games.
        * Controller --> Integrates the model and view so that the game runs properly.
        * GameListener --> Works with the view and model package and the controller class to make GUI buttons (and other user inputs) functional.
    * Model
        * Board --> Holds all the Locations, and therefore is the abstract representation of the game board. Works with the view package to visualize the board and (obviously) works with Location to build itself.
        * Game --> Holds functionality for the gameplay as it relates to the backend. Works with controller and virtually every other back end class to do things such as drawing cards, starting auctions, making trades, landing on spaces...
        * GameStatus --> Enum that states whether the game has yet to start, finished, or is currently being played. Is used by the Game and Controller.
        * Transaction  --> Used to create money transactions between palyers. Communicates with Game and uses Players.
            * Trade (subclass) --> Used to trade properties and money between players. Communicates with Game and PropertySpace and uses Players.
        * (Abstract) Card --> Used as an abstract representation of Chance and Community chest cards. Used by the Game to do actions specified in data files. Written and read to files by the DataReader and DataWriter via the Controller.
            * MoveNearestRailRoadCard
            * MoveNearestUtilityCard
            * MovementCard
            * PaymentCard
        * (Abstract) Location --> Abstract representation of locations on the board. Used by the Game and players for board-player interactions and rent. Used by the view package to visualize the board, and location statistics (rent, houses...). Written and read to files by the DataReader and DataWriter via the Controller.
            * CardSpace
            * CommonSpace
            * JailSpace
            * MoneySpace
            * TeleportSpace
            * (Abstract) PropertySpace --> Abstract representation of ownable properties. These Locations charge rent and can be purchased.
                * RailRoad 
                * RealEstate
                * Utility
        * (Abstract) Player --> Abstract representation of the game players. Used by the game and interacts with locations and Transactions to mediate game play. Used by view to visualize player stats and position. Written and read to files by the DataReader and DataWriter via the Controller.
            * Bank
            * CPUPlayer
            * HumanPlayer
        * Turn --> Models a players turn from dice roll to movement. Used by Game and uses Players, Die, Roll, Rule, Card, Location, and PlayerState to model each turn.
            * CPUTurn (subclass)
        * PlayerState --> Models a player's in game state. For example, a Player may be in pre-roll, in management, rolling, or not their turn... Used by view to visualize what actions are capable by a Player.
        * (Abstract) Die --> Used by Roll, Turn, and in some cases Card to help a player move about the board.
            * NSidedDie
        * Roll --> Contains Die objects to get the full roll and doubles of a Player. Used by Turn.
        * (Abstract) Rule --> Does certain actions in very specific situations. For example, with Location and Player it might reward double the amount of money on go. Written and read to files by the DataReader and DataWriter via the Controller.
            * DoubleMoneyOnGoRule
            * EvenBuildingRule
            * GetMoneyOnFreeParkingRule
            * JailOnThreeDoublesRule
            * LastPlayerStandingWinsRule
            * MoneyOnPassingGoRule
            * RichestPlayerAfterFirstBankruptcyWinsRule
        * (Package) Factory --> Used by the DataReader and Game to construct new objects from Data files.
            *  CardFactory
            *  DieFactory
            *  LocationFactory
            *  PlayerFactory
            *  RuleFactory
            *  TurnFactory
    * View (for the sake of time, I will only write important classes and packages)
        *  (package) panel --> Used by the Controller to make visualizations of panels that hold buttons. Uses Player states to know what panel to use.
        *  (package) popup --> Used by the Controller to give and get information to and from the user.
        *  (package) screen --> Used by the Controller to set up different screens, including splash screens. Information from these screens interact with the Controller to be written and read to and from files.
        *  (package) tilepane --> Used to visualize images from files in the GUI.
        *  BoardPane --> Used to visualize the Board via the Controller.

 * What is needed to represent a specific game variant (such as new code or resources or asset files)?
    * If you want to load in your own game file of any game variant
        1. Make new Locations files. This allows for complete customization of what type of locations and specific statistics. (If you like full customization also upload image files for non real estate spaces).
        2. Make new Player files. Similarly here you can specify how much money, and which properties each player owns...
        3. Make new Card files. This is where you add any and all chance and community chest cards. You can specify what you want the messages to be as well as the associated actions.
        4. Make a new Rule file. This is where you customize which rules you want to play with. The only rule absolutely necessary is a way to win (either lastplayerstanding or richestplayerafterfirstbankrupt).
        5. Lastly, write a file that maps players to their token images.
        6. NOTE: these files need to be all placed in the same folder, and need to be named very specifically. Each location file should be in a subfolder "board". Each Player file should be in a subfolder "players"...
        7. Run the program and press "LOAD GAME". Select the entire folder, and start playing.
    * If you want to be given the choice of a new variant on the choose type of game screen the method is a little more complicated.
        1. Do steps 1 through 6 above to make default files for this new game type.
        2. Add an image and option on the game selection screen.
        3. Link the specific screen with that game type to the correct folder of default settings.
        4. Run the program as normal.
 * What dependencies between the code are clear (e.g., public methods and parameters) and what are through "back channels" (e.g., static calls, order of method call, requirements for specific subclass types)?
    * Most of the class level dependencies are clear from the API description above.
    * Many of our back end classes feature public methods with parameters. Objects are usually fed through by the Game class.
    * Some of our strategies for avoiding dependencies were to get information by public getters and setters as opposed to parameterization. Furhtermore, in Controllore the order of each method call as it relates to splash screens is very specific so that we can take information from the user and write them to file in a way that minimizes dependencies.
    * IntelliJ created dependency diagrams can be seen below. NOTE: I did not include testing in these diagrams.
    * ![View](https://ibb.co/Gpp33nL)
    * ![Model](https://ibb.co/WHVdR9R)
    * ![Controller](https://ibb.co/7tgfx4P)

#### API #1 Rule

 * What helps (or hurts) this API to be flexible and encourage good design in users code?
    * This API is really simple, but very very flexible. It has just two methods, both of which are abstract. One method checks if a given Rule should be enacted. The other does the associated action. It is flexible for both other engineers and personal users. If you are an engineer and want to have the game logic work with rules, all you must do is iterate through a Collection of Rule objects, call a mthod that check if the rule should be done, and from there call the second method to do the action. On the flip side, if you are a user who wants to add a new type of rule, all you need to do is add a new Class extending Rule and implement the two methods.
 * How does this API help (or hurt) encapsulating implementation decisions?
    * This API tells almost nothing to what ever code is using it. It is close to being fully encapsulated. The caller never knows what the criteria for a rule is, nor does it know what action the rule causes.
 * How does this API handle exceptions (error cases) that might occur well (or not)?
    * This API doesn't show off exception cases, as it interacts with the user and files minimally. Anything that goes wrong is encoded in the logic.
 * What have you learned about design (either good or bad) by reading your team mates' code?
    * This is a good example of abstraction and polymorphism. It allows for easy extension and flexibility of code elsewhere in the program while reducing duplicated code. The only thing is that it could easily be changed to an interface. This class only describes what cards do, with no mention of instance variables. Functionally, it behaves the same as an interface, however it allows for less extension by being an abstract class (although I am unsure of why you would want Rule obkects to implement both Rule and another interface/class).

#### API #2 FrontEndDataReader

 * What helps (or hurts) this API to be flexible and encourage good design in users code?
    * This API is part of another abstraction, that of DataReader. However, the goal of the FrontEndDataReader was not necessarily to be the most flexible and extendible. The code is pretty easy to modify if you wanted to add a new front end data driven aspect, but this would still require going into the actual code and adding methods.
 * How does this API help (or hurt) encapsulating implementation decisions?
    * This API has good encapsulation. It is simply full of readers and getters. The readers require no more information than a filepath to a game and the getters all flexibly return Map objects. Any class calling methods that reside in the FrontEndDataReader give it minimal information, and get the information that they need without any knowledge of how the Reader operates.
 * How does this API handle exceptions (error cases) that might occur well (or not)?
    * This API throws IO exceptions in the event that any method is passed in a file path that does not lead to a valid file. These exceptions are then caught in the Controller and front end, where the user is not allowed to continue without selecting the correct file types. When these exceptions are thrown, the program does not break, but instead waits for appropriate action.
 * What have you learned about design (either good or bad) by reading your team mates' code?
    * This API shows off some of the small powers of inheritance and abstraction, but the real design is in the error handling. This is a textbook IO class with error checking where appropriate. It is very helpful in learning how to write software that refuses to break when a user gives the wrong input.

## Your Design

 * Describe how your code is designed at a high level (focus on how the classes relate to each other through behavior (methods) rather than their state (instance variables)).
    * Controller --> This class interacts with files via method calls in the FrontEnd and BackEnd Data Readers. Furthermore, the entire front end and back end are connected here.
    * DataWriter --> This class interacts only with the controller. 
    * DataReader --> An abstraction heirarchy leads to reduced duplicate code between the Front and BackEnd DataReaders.
    * BackEndDataReader --> Similarly to the writer this class interacts only with the Controller.
    * Location --> This class interacts heavily with Players. Method calls in Turn cause players to change locations and interact with said locations. Furthermore, abstraction allows for the Board to be built of multiple different types, which in turn allows for polymorphism to help implement functionality.
    * PropertySpace --> This class interacts more heavily with Players. Houses/hotels, rent, trading, buying, mortgaging, etc. all happen via method calls that interact between the Players and PropertySpace. In the same vein as Location, PropertySpace is abstract, allowing its subclasses to implement the same methods in different manners.
        * RealEstates --> All of these three interact with the player. Their main difference is in their updating of their rent.
        * RailRoad
        * Utility
    * Transaction --> This class interacts with players. Methods called in Game create new transactions.
    * Trade --> Similarly to Transactions, Trade interacts with both Players and PropertySpaces to make trading work.
 * Discuss any Design Checklist issues within your code (justify why they do not need to be fixed or describe how they could be fixed if you had more time).
    * One issue is in a conditional statement that checks if two strings are equal within the DataWriter. This comes into play when writing location files. PropertySpaces have very differeny data written to their files than other Locations. Therefore, it is important to know whether a given Location should be written as a PropertySpace or a regular Location. We compare their class paths via reflection to a Strings to see if the given location is a PropertySpace (the code can be seen below).
```java
    /**
     * Writes a location file from an already constructed location
     * @param location a Location object that is to be written
     */
    public void makeLocationFile(Location location) {
        myProp = new Properties();
        String filename = join(location.getName().toLowerCase().split(" "), "_");
        String type = location.getClass().toString();
        String[] classPath = (type.split(" ")[1]).split("\\.");
        type = classPath[classPath.length - 1].toUpperCase();

        if (type.equalsIgnoreCase("REALESTATE") || type.equalsIgnoreCase("UTILITY") ||
                type.equalsIgnoreCase("RAILROAD")) {
                    ...
```
   * One clear flaw in the above code is the use of magic values. Later on I will discuss the possibility of a user adding a new type of property, but now let's consider simply changing the name of the Utility Class. If one were to do that, one would also have to update the magic value String in the conditional shown above. This is not flexible code.
   * I actually forgot to push a better version of this code on my own branch. Here is the following code that lived on my local computer:
```java
    String type = location.getClass().toString();
    String[] classPath = (type.split(" ")[1]).split("\\.");
    type = classPath[classPath.length - 1].toUpperCase();

    if (classPath[classPath.length - 2].equalsIgnoreCase(PROPERTY)) {
        ...
```
* 
    * This code is superior and can be seen currently on my branch. It is better because in this case, if a user were to add an extra type of PropertySpace, the correct information would still be written. 
    * That being said, one downside it does have is that any new type of Location that is not a PropertySpace but still requires extra data can not be written as the code is written. One possible work around of this would be to make new "data-classes" such as DataPropertySpace or DataLocation. These classes would hold all the necessary information to be written, and a call to one method by the DataWriter could get a Collection of Map.Entry objects to be written. It would then simply iterate through the collection, writing each entry. These data classes would have to be separate from the regular back end classes because the file handling should be almost entirely closed to the back end.
    * Ultimately, I decided against the data-class route because there have to be constraints on what a user can do on some level in order for the game to still be Monopoly. A new type of Property was more reasonable to me, hence the change of code that I forgot to push. However a totally new type of Location that required more data seemed too far out of the realm of Monopoly to be considered in design. The latter part of the code is the happy middle ground between generalized and flexible code while still being constrained to an actual game of Monopoly.
    
#### Feature #1 (good) PropertySpace

 * Justify why the code is designed the way it is or what issues you wrestled with that made the design challenging.
    * The code is abstract and simple. This was super helpful elsewhere in the programming, as all the code was generalized to work on any Property as opposed to specific types of Properties. The entirety of the heirarchy of Locations lended itself perfectly to have flexibility and encapsulation for both the front end and back end. It is excellent design.
    * I think the best design is elegantly simple. Similarly, we found very little problems in designing PropertySpaces. It is a little long for a class, but other than that, there were few design struggles.
 * Are there any assumptions or dependencies from this code that impact the overall design of the program? If not, how did you hide or remove them?
    * The only real depencies that exist in this code are with Player. Several methods require a Player object to be passed through as parameters. Furthermore, dice rolls are sometimes required to be passed through in a similar fashion. Besides that, they act largely independetly of other classes.

#### Feature #2 (could be improved) DataWriter

 * Justify why the code is designed the way it is or what issues you wrestled with that made the design challenging.
    * For the msot part this code is very closed to the public, is amply generalized, and handles errors approriately. However, the aformentioned design checklist issue exists.
    * As I wrote about earlier, it was a design decision to not have the data based classes for easy writing of data.
 * Are there any assumptions or dependencies from this code that impact the overall design of the program? If not, how did you hide or remove them?
    *  The class makes certain assumptions about the data and file system, however all of these assumptions are checked and appropriate exceptions are thrown.
    *  The class does have some dependencies, as it gives the Controller the ability to pass through Player, Location, Rule, or Card objects to be written. That being said, there is little way to get around this, as their data that changes within the game needed to be written to file. Ultimately, this is far far from terribly written code (in my opinion), but for the reasons discussed above, it could be better.

## Flexibility

 * Describe how the final API that you were most involved with balances Power (flexibility and helping users to use good design practices) and Simplicity (ease of understanding and preventing users from making mistakes).
    * Our final API is almost entirely data driven. Therefore, in order for a user to customize a game to their hearts content, all they need to do is manipulat data files. This is the power of strong flexible design. On the flip side, each of the data files are relatively easy to read and write, and error checking makes sure they have the appropriate information.


#### Feature #1 (good) Card (and its heirarchy)

 * What is interesting about this code (why did you choose it)?
    * I think this heirarchy shows the beneifits of abstraction and polymorphism, as it easily allows for different types of cards.
 * What classes or resources are required to implement this feature?
    * Card objects require only data from files and are dependent on Players who land on them. Besides that, they make no assumptions and operate completely closed to the game.
 * Describe the design of this feature in detail (what parts are closed? what implementation details are encapsulated? what assumptions are made? do they limit its flexibility?).
    * There is an abstract class Card that outlines the functionality for any type of card. It has certain universal functionality, such as checking if an action is done or not and if the card is holdable by the player. There are four subclasses, each one detailing the functionality for a specific type of card. For example, a movement card simply moves yu to a location. A payment card either gives or takes money etc.
    * The cards are almost completely closed and are completely encapsulated. Furthermore, they make very few if any assumptions. It is clean, sleak code.
 * How extensible is the design for this feature (is it clear how to extend the code as designed or what kind of change might be hard given this design)?
    * The code is very extensible in one sense, and not very in another.
    * If a user wants to create a new type of card, that is very doable. It is as simple as writing the class and extending the super class. The only other step would be to write its configuration files.
    * The less extensible feature is making cards that combine types. For example, we have separate Card classes for moving to the nearest utility and railroad even though these are simply specific combinations of movement and payment cards. 

#### Feature #2 (could be improved) BoardPane

 * What is interesting about this code (why did you choose it)?
    * I chose this code simply because it I was knowledgeable about its assumptions and had previously discussed its design with its author.
 * What classes or resources are required to implement this feature?
    * The only real reasource needed is a Location and the number of locations on the board.
 * Describe the design of this feature in detail (what parts are closed? what implementation details are encapsulated? what assumptions are made? do they limit its flexibility?).
    * This code visuaizes the board on the GUI. It is not terrible, but also not ideal. There are several switch statements that are less than perfect, the code is not particularly readable (an if statement inside two nested for loops), there is very limited commenting, and there is a large assumption that the number of locations on the board will be divisible by four.
    * A large topic of design that I often talked about with the author was wether the class should be dependent on the Board or just a Location. By making a linked list of Locations within Board, every Location has a next and previous location. Therefore when making the board, all you need is one Location and then you can itearate through. This increases encapsulation, as instead of BoardPane knowing the whole Board object and all its methods, it only needs a Location and said Location's next method.
 * How extensible is the design for this feature (is it clear how to extend the code as designed or what kind of change might be hard given this design)?
    * The main way to extend the visualization of the board (that I can think of) is to allow for easy changing of board shape. 
    * As the code is currently designed, we are not set up for this challenge. We can build rectangular boards and that is it.
    * Furthermore, because the placement of each tile is determined by shuffling it into its appropriate side in a switch statement, it is unclear how the class could be extended to build circular or triangular boards (or even boards with number of locations not divisible by four).
    * This could be greatly improved by an abstraction, where the placement method of each tile differs for the shape of the board.


## Alternate Designs

#### API that Changed Rules

 * Why was it changed and how much impact did the changes have on other parts of the project?
    * Initially our API for Rule was a long class with specific methods to implement every different possible rule.
    * This quickly was changed to our current API which is abstraced.
    * The change definitely helped our project. I am unsure if our old API even would have worked. Furthermore, our current API is way more extendable, better organized, and more understandable. It did not require a huge amount of refactoring because we made the change before implementing the rules.
 * How were the changes discussed and the decisions ultimately made?
    * The changes were discussed between Leah and myself, and then Leah implemented them.
 * Do you feel the changes improved the API (or not) (i.e., did they make the API more abstract or more concrete? more encapsulated or not? more flexible or not?)?
    * As stated earlier, the change created both more abstraction and made the code more open to extension.

#### Design Decision #1 Locations

 * What alternate designs were considered?
    * Early on in the process we thought we would have an abstraction heirarchy for Locations and an Ownable Interface (to be discussed next). Properties would extend Location and inherit Ownable. We would have specific Location intances such as Jail, and Go, and Tax...
        * The benefit of this design was that Players could own one collection of Ownables including both their properties and get out of jail cards. This would also improve the ease of trading.
    * Quickly, we ditched the Ownable interface. We also wanted the Locations to be more flexible. For example, we wanted the capabilities for a given location to be both a property and a tax for example.
    * Therefore, we pivoted to a concrete Location class that held an abstract SpaceType. These types were such things as moneySpace (for giving and taking money), transportationSpace (such as go to jail)... However, we quickly realized the difficulty in implementing this and agreed that it was reasonable for locations to be only one type.
    * We switched to out final design, a hybrid of the two previous designs. An abstract Location, with more generalized concrete instances. Furthermore, we had another abstraction within Location of PropertySpace objects. RailRoads, RealEstates, and Utilitys all extend this abstract class.
 * What are the trade-offs of the design choice (describe the pros and cons of the different designs)?
    * The main pro of the current design is that is flexible, general, works well with data driven design and the rest of the project, and is closed and encapsulated to most of the project.
    * The first design was dumped for reasons to be explained below.
 * Which would you prefer and why (it does not have to be the one that is currently implemented)?
    * I definitely prefer the current implementation.

#### Design Decision #2 Ownable Interface

 * What alternate designs were considered?
    * Early on in the design, we expected to have an Ownable interface for specific cards and Locations. The thought was that players could own one collection of Ownables, making trading far easier. 
    * This design was dumped completely. Location simply had PropertySpace objects that could be owned and Cards have their own separate method isOwnable.
 * What are the trade-offs of the design choice (describe the pros and cons of the different designs)?
    * The initial design had significant problems. Firstly and most importantly, trading seemed to be the only thing it improved. There were several times that a player would have to iterate through their properties, but it would be impossible to iterate through one collection of Ownables for just the properties without type casting. Therefore, the player would already separate their assets into two groups. The only shared functionality was in trading, and while this could've justified the interface, we utlimately struggled with how to enact get out of jail free cards (the only traditionally tradeable card). Therefore, we dropped the interface altogether.
 * Which would you prefer and why (it does not have to be the one that is currently implemented)?
    * Once again, I prefer our current design.


## Conclusions

 * Describe the best feature of the project's current design and what did you learn from reading or implementing it?
    * I think the design of our board is really good. It is a great example of abstraction and generalization of code. Especially as it works so well to inform both the back end and the front end. 
 * Describe the worst feature that remains in the project's current design and what did you learn from reading or implementing it?
    * There were a lot of portions of the front end that featured hard coding. This makes it way less flexible and extendable. That being said, reading it was a learning experience as I have relatively limited experience with JavaFX. Seeing different parts of front end code that were designed well and not well was interesting because the deisgn is a little bit different from the back end (although joined by the same principles).
 * What is your biggest strength as a coder/designer?
    * I think my biggest strength is that I am able to write very organized, readable code.
 * What is your favorite part of coding/designing?
    * I love the process of object-oriented design. By the end of the semester, I genuinely enjoyed the problem solving of how all the parts should work together in the best way. This is a kind of broad answer, but the early design stages I enjoy and I think I accel at.
 * What are two specific things you have done this semester to improve as a programmer this semester?
    * Something as simple as writing java codes from scratch was immensely helpful to my understanding of the language. Previously I had never done that.
    * I coded with other people via git. I had also never done this before, and it is immediately clear that this is challenging but also the only way to launch large-scale projects. It also forces you to write code that other people can understand easily.
    * Also writing API's was super useful to how I coded.

